// Copyright 2013 Velodyne Acoustics, Inc.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
#include "lqVeloViewManager.h"

#include "lqPythonQtVeloView.h"
#include "lqOpenPcapReaction.h"

#include <pqActiveObjects.h>
#include <pqRenderView.h>

#include "vtkSMPropertyHelper.h"

#include <QFileInfo>

//-----------------------------------------------------------------------------
lqVeloViewManager::lqVeloViewManager(QObject* parent /*=nullptr*/)
  : Superclass(parent)
{

}

//-----------------------------------------------------------------------------
lqVeloViewManager::~lqVeloViewManager()
{

}

//-----------------------------------------------------------------------------
void lqVeloViewManager::pythonStartup()
{
  // Register LidarView specific decorators first
  PythonQt::self()->addDecorators(new lqPythonQtVeloView(this));

  Superclass::pythonStartup();

  // Alias vv
  this->runPython(QString("import lidarview.applogic as vv\n"));
}
